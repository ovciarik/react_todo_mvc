import React, { useEffect, useState, useRef } from 'react';
import { v4 as uuidv4 } from 'uuid';
import ClipLoader from "react-spinners/ClipLoader";


import _ from 'lodash';

const syncTodoItems = _.debounce(async (todoItems) => {
    try {
        const res = await fetch(
            '/api/todos',
            {
                method: 'POST',
                mode: 'cors',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(todoItems),
            },
        )
        if (!res.ok) {
            throw await res.text()
        }
    } catch (err) {
        console.log(err)
    }
}, 250)

async function createTodoItem(todoItem: Item) {
    try {
        const res = await fetch(
            '/api/todoItem',
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(todoItem),
            },
        )
        if (!res.ok) {
            throw await res.text()
        }
    } catch (err) {
        console.log(err)
    }
}

async function updateTodoItems(todoItems: Item[]) {
    try {
        const res = await fetch(
            '/api/todoItems',
            {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(todoItems),
            },
        )
        if (!res.ok) {
            throw await res.text()
        }
    } catch (err) {
        console.log(err)
    }
}

async function deleteTodoItems(todoItems: Item[]) {
    try {
        const res = await fetch(
            '/api/todoItems',
            {
                method: 'DELETE',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(todoItems),
            },
        )
        if (!res.ok) {
            throw await res.text()
        }
    } catch (err) {
        console.log(err)
    }
}


const updateTodoItem = _.debounce(async (todoItem: Item) => {
    try {
        const res = await fetch(
            '/api/todoItem',
            {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(todoItem),
            },
        )
        if (!res.ok) {
            throw await res.text()
        }
    } catch (err) {
        console.log(err)
    }
}, 250)

async function deleteTodoItem(todoItem: Item) {
    try {
        const res = await fetch(
            '/api/todoItem',
            {
                method: 'DELETE',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(todoItem),
            },
        )
        if (!res.ok) {
            throw await res.text()
        }
    } catch (err) {
        console.log(err)
    }
}

type Item = {
    _id: string,
    value: string,
    isDone: boolean,
}


function MainInput({ todoItems, setTodoItems, setShouldSync }: { todoItems: Item[], setTodoItems: React.Dispatch<React.SetStateAction<Item[]>>, setShouldSync: any }) {
    // console.log('render MainInput')
    const [inputState, setInputState] = useState('')

    async function commitInput(value: string) {
        const newItem = { value, isDone: false, _id: uuidv4() }
        setInputState('')
        setTodoItems([...todoItems, newItem])
        // setShouldSync(true)
        await createTodoItem(newItem)
    }


    async function markAll() {
        // if all marked true
        const items = todoItems
        const newValue = (items.length > 0 && !items.map((x) => x.isDone).reduce((a, b) => a && b, true))
            ? items.map((item) => { return { ...item, isDone: true } })
            : items.map((item) => { return { ...item, isDone: false } })
        setTodoItems(newValue)
        await updateTodoItems(newValue)
    }

    return <div className='border-b border-stone-600 p-2 space-x-2'>
        <button className='mdi mdi-chevron-down' onClick={markAll}></button>
        <input
            className='bg-stone-700 focus-visible:outline-0'
            value={inputState}
            onChange={(e) => setInputState(e.target.value)}
            placeholder='What needs to be done ?'
            autoFocus
            onKeyDown={(e) => { e.key === 'Enter' && commitInput(inputState) }}
        ></input>
    </div >
}


function TodoItems({ todoItems, setTodoItems, filter, setShouldSync }: { todoItems: Item[], setTodoItems: React.Dispatch<React.SetStateAction<Item[]>>, filter: string, setShouldSync: any }) {
    // console.log('render TodoItems')
    const [editedCell, setEditedCell] = useState(-1)
    const inputRef = useRef<HTMLInputElement>(null)

    useEffect(() => {
        if (inputRef.current !== null) {
            inputRef.current.focus()
        }
    }, [editedCell])

    async function handleDeleteItem(idx: number) {
        setTodoItems([...todoItems.slice(0, idx), ...todoItems.slice(idx + 1)])
        await deleteTodoItem(todoItems[idx])
    }

    async function handleUpdateItemIsDone(e: React.ChangeEvent<HTMLInputElement>, idx: number) {
        const newItem = { ...todoItems[idx], isDone: e.target.checked }
        setTodoItems([...todoItems.slice(0, idx), newItem, ...todoItems.slice(idx + 1)])
        await updateTodoItem(newItem)
    }

    async function handleUpdateItemValue(e: React.ChangeEvent<HTMLInputElement>, idx: number) {
        const newItem = { ...todoItems[idx], value: e.target.value }
        setTodoItems([...todoItems.slice(0, idx), newItem, ...todoItems.slice(idx + 1)])
        await updateTodoItem(newItem)
    }

    return <div className='' >
        {todoItems.map((item, idx) => ((filter === 'all') || (filter === 'active' && !item.isDone) || (filter === 'completed' && item.isDone)) && <div
            key={idx}
            className={`flex justify-between items-center group ${idx !== editedCell ? 'border-b' : ''} border-stone-600`}
            onDoubleClick={() => setEditedCell(idx)}
        >
            <input
                type='checkbox'
                className={`mx-2 ${idx === editedCell ? 'invisible' : ''}`}
                checked={item.isDone}
                onChange={(e) => handleUpdateItemIsDone(e, idx)}
                onDoubleClick={(e) => e.stopPropagation()}
            ></input>

            {idx !== editedCell ? <div
                className={`p-2 bg-stone-700 flex-1 text-left ${item.isDone ? 'line-through text-stone-600' : ''}`}
            >{item.value}
            </div> : <input
                className={`p-2 border border-1 border-stone-600 bg-stone-700 focus-visible:outline-0 flex-1 text-left`}
                onBlur={() => setEditedCell(-1)}
                onChange={(e) => handleUpdateItemValue(e, idx)}
                onKeyDown={(e) => { e.key === 'Enter' && setEditedCell(-1) }}
                value={item.value}
                ref={inputRef}
            >
            </input>}

            <button
                className={`mdi mdi-close invisible p-2 ${idx === editedCell ? 'hidden' : 'group-hover:visible'}`}
                onClick={() => handleDeleteItem(idx)}
            ></button>
        </div>
        )}
    </div>
}

function Filters({ todoItems, filter, setFilter, setTodoItems }: { todoItems: Item[], filter: string, setFilter: React.Dispatch<React.SetStateAction<string>>, setTodoItems: React.Dispatch<React.SetStateAction<Item[]>> }) {
    // console.log('render Filters')
    const itemCount = todoItems.map((x): number => x.isDone ? 0 : 1).reduce((a, b) => { return a + b }, 0)

    async function handleDeleteItems() {
        const toDelete = todoItems.filter((x) => x.isDone)
        setTodoItems((items) => items.filter((x) => !x.isDone))
        await deleteTodoItems(toDelete)
    }

    return <div className='p-2'>
        <span className='inline-block pr-2 w-24'>{`${itemCount} item${itemCount === 1 ? '' : 's'} left`}</span>
        <span className='m-2'>
            <input
                type="radio"
                name="filters-all"
                value="all"
                checked={filter === 'all'}
                onChange={(e) => setFilter(e.target.value)}
            /><label htmlFor='filters-all' className={`border ${filter === 'all' ? 'border-stone-600' : 'border-stone-700'}`}>All</label>

            <input
                type="radio"
                name="filters-active"
                value="active"
                checked={filter === 'active'}
                onChange={(e) => setFilter(e.target.value)}
            /><label htmlFor='filters-active' className={`border ${filter === 'active' ? 'border-stone-600' : 'border-stone-700'}`}>Active</label>


            <input
                type="radio"
                name="filters-completed"
                value="completed"
                checked={filter === 'completed'}
                onChange={(e) => setFilter(e.target.value)}
            /><label htmlFor='filters-completed' className={`border ${filter === 'completed' ? 'border-stone-600' : 'border-stone-700'}`}>Completed</label>

        </span>

        <button
            className={`${itemCount === todoItems.length ? 'invisible' : ''}`}
            onClick={handleDeleteItems}
        >Clear completed</button>
    </div >
}


export default function TodoList() {
    const [todoItems, setTodoItems] = useState<Item[]>([])
    const [filter, setFilter] = useState('all')
    const [shouldSync, setShouldSync] = useState(false)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        console.log('effect init')
        fetch(
            '/api/todoItems',
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },

            },
        ).then((response) => {
            return response.json()
        }).then((r) => {
            setTodoItems(r)
            setLoading(false)
        }).catch((e) => {
            console.log(e)
        })
    }, [])

    useEffect(() => {
        // console.log('effect sync')
        if (shouldSync) {
            console.log('sync state')
            syncTodoItems(todoItems)
        } else {
            // console.log('skip sync')
        }
    }, [todoItems, shouldSync])

    return <div className='flex justify-center items-center min-h-screen'>
        <div className='flex flex-col justify-center'>
            {loading && <ClipLoader color='#0000FF'></ClipLoader>}
            {!loading && <div className='bg-stone-700 shadow-2xl'>
                <MainInput todoItems={todoItems} setTodoItems={setTodoItems} setShouldSync={setShouldSync} ></MainInput>
                <TodoItems todoItems={todoItems} setTodoItems={setTodoItems} setShouldSync={setShouldSync} filter={filter} ></TodoItems>
                <Filters todoItems={todoItems} filter={filter} setFilter={setFilter} setTodoItems={setTodoItems} ></Filters>
            </div>}
        </div>
    </div>
}