import React, { useEffect, useState, useRef } from 'react';
import ClipLoader from "react-spinners/ClipLoader";
import _ from 'lodash';

type Item = {
    value: string,
    isDone: boolean,
}

const syncTodoItems = _.debounce(async (todoItems) => {
    try {
        const res = await fetch(
            '/api/todos',
            {
                method: 'POST',
                mode: 'cors',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                body: JSON.stringify(todoItems),
            },
        )
        if (!res.ok) {
            throw await res.text()
        }
    } catch (err) {
        console.log(err)
    }
}, 250)

function useTodoState(): [Item[] | null, (_: Item[]) => void] {
    const [_todoItems, _setTodoItems] = useState<Item[] | null>(null)

    useEffect(() => {
        fetch(
            '/api/todos',
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },

            },
        ).then((response) => {
            return response.json()
        }).then((r) => {
            _setTodoItems(r)
        }).catch((e) => {
            console.log(e)
        })
    }, [])

    const setTodoItems = (todoItems: Item[]) => {
        _setTodoItems(todoItems)
        syncTodoItems(todoItems)
    }

    return [
        _todoItems,
        setTodoItems
    ]
}

function MainInput({ todoItems, setTodoItems }: { todoItems: Item[], setTodoItems: (todoItems: Item[]) => void }) {
    const [inputState, setInputState] = useState('')

    const commitInputHandler = (value: string) => {
        setInputState('')
        setTodoItems([...todoItems, { value, isDone: false }])
    }

    const markAllHandler = () => {
        // if all marked true
        const items = todoItems
        const newValue = (items.length > 0 && !items.map((x) => x.isDone).reduce((a, b) => a && b, true))
            ? items.map((item) => { return { ...item, isDone: true } })
            : items.map((item) => { return { ...item, isDone: false } })
        setTodoItems(newValue)
    }

    return <div className='border-b border-stone-600 p-2 space-x-2'>
        <button className='mdi mdi-chevron-down' onClick={markAllHandler}></button>
        <input
            className='bg-stone-700 focus-visible:outline-0'
            value={inputState}
            onChange={(e) => setInputState(e.target.value)}
            placeholder='What needs to be done ?'
            autoFocus
            onKeyDown={(e) => e.key === 'Enter' && commitInputHandler(inputState)}
        ></input>
    </div >
}


function TodoItems({ todoItems, setTodoItems, filter }: { todoItems: Item[], setTodoItems: (todoItems: Item[]) => void, filter: string }) {
    const [editedCell, setEditedCell] = useState(-1)
    const inputRef = useRef<HTMLInputElement>(null)

    useEffect(() => {
        if (inputRef.current !== null) {
            inputRef.current.focus()
        }
    }, [editedCell])

    return <div className='' >
        {todoItems.map((item, idx) => ((filter === 'all') || (filter === 'active' && !item.isDone) || (filter === 'completed' && item.isDone)) && <div
            key={idx}
            className={`flex justify-between items-center group ${idx !== editedCell ? 'border-b' : ''} border-stone-600`}
            onDoubleClick={() => setEditedCell(idx)}
        >
            <input
                type='checkbox'
                className={`mx-2 ${idx === editedCell ? 'invisible' : ''}`}
                checked={item.isDone}
                onChange={(e) => setTodoItems([...todoItems.slice(0, idx), { ...todoItems[idx], isDone: e.target.checked }, ...todoItems.slice(idx + 1)])}
                onDoubleClick={(e) => e.stopPropagation()}
            ></input>
            {idx !== editedCell ? <div
                className={`p-2 bg-stone-700 flex-1 text-left ${item.isDone ? 'line-through text-stone-600' : ''}`}
            >{item.value}
            </div> : <input
                className={`p-2 border border-1 border-stone-600 bg-stone-700 focus-visible:outline-0 flex-1 text-left`}
                onBlur={() => setEditedCell(-1)}
                onChange={(e) => setTodoItems([...todoItems.slice(0, idx), { ...todoItems[idx], value: e.target.value }, ...todoItems.slice(idx + 1)])}
                onKeyDown={(e) => e.key === 'Enter' && setEditedCell(-1)}
                value={item.value}
                ref={inputRef}
            >
            </input>}
            <button
                className={`mdi mdi-close invisible p-2 ${idx === editedCell ? 'hidden' : 'group-hover:visible'}`}
                onClick={() => setTodoItems([...todoItems.slice(0, idx), ...todoItems.slice(idx + 1)])}
            ></button>
        </div>
        )}
    </div>
}

function Filters({ todoItems, filter, setFilter, setTodoItems }: { todoItems: Item[], filter: string, setFilter: React.Dispatch<React.SetStateAction<string>>, setTodoItems: (todoItems: Item[]) => void }) {
    // console.log('render Filters')
    const itemCount = todoItems.map((x): number => x.isDone ? 0 : 1).reduce((a, b) => { return a + b }, 0)

    return <div className='p-2'>
        <span className='inline-block pr-2 w-24'>{`${itemCount} item${itemCount === 1 ? '' : 's'} left`}</span>
        <span className='m-2'>
            <input
                type="radio"
                name="filters-all"
                value="all"
                checked={filter === 'all'}
                onChange={(e) => setFilter(e.target.value)}
            /><label htmlFor='filters-all' className={`border ${filter === 'all' ? 'border-stone-600' : 'border-stone-700'}`}>All</label>
            <input
                type="radio"
                name="filters-active"
                value="active"
                checked={filter === 'active'}
                onChange={(e) => setFilter(e.target.value)}
            /><label htmlFor='filters-active' className={`border ${filter === 'active' ? 'border-stone-600' : 'border-stone-700'}`}>Active</label>
            <input
                type="radio"
                name="filters-completed"
                value="completed"
                checked={filter === 'completed'}
                onChange={(e) => setFilter(e.target.value)}
            /><label htmlFor='filters-completed' className={`border ${filter === 'completed' ? 'border-stone-600' : 'border-stone-700'}`}>Completed</label>
        </span>
        <button
            className={`${itemCount === todoItems.length ? 'invisible' : ''}`}
            onClick={() => setTodoItems(todoItems.filter((x) => !x.isDone))}
        >Clear completed</button>
    </div >
}

export default function TodoList() {
    const [filter, setFilter] = useState('all')
    const [todoItems, setTodoItems] = useTodoState()

    return <div className='flex justify-center items-center min-h-screen'>
        <div className='flex flex-col justify-center'>
            {todoItems === null
                ? <ClipLoader color='#0000FF'></ClipLoader>
                : <div className='bg-stone-700 shadow-2xl'>
                    <MainInput todoItems={todoItems} setTodoItems={setTodoItems}  ></MainInput>
                    <TodoItems todoItems={todoItems} setTodoItems={setTodoItems} filter={filter} ></TodoItems>
                    <Filters todoItems={todoItems} filter={filter} setFilter={setFilter} setTodoItems={setTodoItems} ></Filters>
                </div>}
        </div>
    </div>
}