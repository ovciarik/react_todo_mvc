import React from 'react';
import TodoList from './components/todo_list_bg_sync';

function App() {
  return (
    <div className='bg-stone-900 text-stone-300 min-h-screen'>
      <TodoList></TodoList>
    </div>
  );
}

export default App;
