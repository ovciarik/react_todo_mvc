import { MongoClient } from "mongodb"
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);

let dbConnection: any;


export const connectToServer = function (callback: any) {
    client.connect(function (err: any, db: any) {
        if (err || !db) {
            return callback(err);
        }

        dbConnection = db.db("test");
        console.log("Successfully connected to MongoDB.");

        return callback();
    });
}

export const getDb = function () {
    return dbConnection;
}