import express from 'express';
import { Request, Response } from 'express';
import { getDb, connectToServer } from './db/conn'

const COLLECTION = 'ajajajaj'
const port = 3001

const app = express()
app.use(express.json());
connectToServer(() => { })

app.get('/api/todos', (req: Request, res: Response) => {

    const dbConnect = getDb();

    dbConnect
        .collection(COLLECTION)
        .find({ 'name': 'ajajaj' })
        .toArray(function (err: any, result: any) {
            if (err) {
                res.status(400).send("Error fetching listings!");
            } else {
                res.json(result[0]?.data)
            }
        });
    res.header('Access-Control-Allow-Origin', '*');
})

app.post('/api/todos', (req: Request, res: Response) => {
    const reqBody = req.body
    const dbConnect = getDb();
    dbConnect
        .collection(COLLECTION)
        .updateOne({ 'name': 'ajajaj' }, { $set: { name: 'ajajaj', data: reqBody } }, { upsert: true })
})
// --------------------------------------------------------------------------------
// todoItems
// --------------------------------------------------------------------------------

app.get('/api/todoItems', (req: Request, res: Response) => {
    const dbConnect = getDb();
    dbConnect
        .collection(COLLECTION)
        .find()
        .toArray(function (err: any, result: any) {
            if (err) {
                res.status(400).send("Error fetching listings!");
            } else {
                res.json(result)
            }
        });
})

app.put('/api/todoItems', (req: Request, res: Response) => {
    const reqBody = req.body
    console.log("app.put('/api/todoItems'")
    console.log(reqBody)
    const dbConnect = getDb();
    reqBody.forEach((elem: any) => {
        dbConnect
            .collection(COLLECTION)
            .updateOne({ _id: elem._id }, { $set: elem })
    })
    res.status(200)
    res.json({})
})

app.delete('/api/todoItems', (req: Request, res: Response) => {
    const reqBody = req.body
    console.log("app.put('/api/todoItems'")
    console.log(reqBody)
    const dbConnect = getDb();
    reqBody.forEach((elem: any) => {
        dbConnect
            .collection(COLLECTION)
            .deleteOne({ _id: elem._id }, { $set: elem })
    })
    res.status(200)
    res.json({})
})

app.post('/api/todoItem', (req: Request, res: Response) => {
    const reqBody = req.body
    console.log("app.post('/api/todoItem'")
    console.log(reqBody)
    const dbConnect = getDb();
    dbConnect
        .collection(COLLECTION)
        .insertOne(reqBody)
    res.status(200)
    res.json({})
})

app.put('/api/todoItem', (req: Request, res: Response) => {
    const reqBody = req.body
    console.log("app.put('/api/todoItem'")
    console.log(reqBody)
    const dbConnect = getDb();
    dbConnect
        .collection(COLLECTION)
        .updateOne({ _id: reqBody._id }, { $set: reqBody })
})

app.delete('/api/todoItem', (req: Request, res: Response) => {
    const reqBody = req.body
    console.log("app.delete('/api/todoItem'")
    console.log(reqBody)
    const dbConnect = getDb();
    dbConnect
        .collection(COLLECTION)
        .deleteOne({ _id: reqBody._id })
})

// --------------------------------------------------------------------------------

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})